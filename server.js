var connect = require('connect'), 
	http = require('http'),
	fs = require('fs'),
	path = require("path"),
	url = require("url");

var server = connect()
	.use(connect.static('app'))
	.use('/js/lib/', connect.static('node_modules/'))
	.use('/node_modules/', connect.static('node_modules/'));

http.createServer(server).listen(8000, function() {
	console.log('Server is running on http://localhost:8000');
});