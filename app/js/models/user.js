define(['underscore',
		'backbone'
], function(_, Backbone) {
	var userDetailsModel = Backbone.Model.extend({
		defaults: {
			name: "Vitalya",
			position: "Macio",
			car: "Mustang",
			fire: "Psh-Psh"
		}
	});

	return userDetailsModel;
});