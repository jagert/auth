requirejs.config({
    baseUrl: '.',
    paths: {
        jquery: 'js/lib/jquery/jquery.min',
        underscore: 'js/lib/underscore/underscore',
        backbone: 'js/lib/backbone/backbone',
        jsonjs: 'js/lib/json/json2',
        text: 'js/lib/require/text',
        app: 'js/app',
        router: 'js/router',
        views: 'js/views',
        collections: 'js/collections',
        models: 'js/models'
    },
    shim: {
        'underscore': {
            exports: '_'
        },
        'jquery': {
            exports: '$'
        },
        'backbone': {
            deps: [
                "underscore",
                "jquery"
            ],
            exports: "Backbone"
        }
    }
});

requirejs(['app'], function(app) {
    app.run();
}); 