define(['jquery',
		'underscore',
		'backbone',
		'text!templates/login.html'
], function($, _, Backbone, loginPageTpl) {
	var LoginPageView = Backbone.View.extend({
		el: $('#container'),
		render: function(){
			var compiledTemplate = _.template(loginPageTpl, {});
			this.$el.append(compiledTemplate);
		}
	});

	return LoginPageView;
});