define(['jquery',
		'underscore',
		'backbone',
		'collections/users',
		'text!templates/user/details.html'
], function($, _, Backbone, usersCollection, userDetailsTpl) {
	var UserDetailsView = Backbone.View.extend({
		el: $('#container'),
		render: function() {
			this.collection = new usersCollection();
			console.log('coll', this);
			// this.collection.add({});
			var compiledTemplate = _.template(userDetailsTpl, {users: this.collection.models});
			this.$el.html( compiledTemplate );
		}
	});

	return UserDetailsView;
});