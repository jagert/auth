define(['router'
], function(Router) {
	var init = function() {
		Router.init();
	};

	return {
		run: init
	};
});