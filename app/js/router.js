define(['backbone',
		'views/user/detailsView',
		'views/login'
], function(Backbone, UserDetailsView, LoginPageView) {
	var Router = Backbone.Router.extend({
		routes: {
			"login":	"showLoginPage",
			"details":	"showUserDetails",
			"*path": "showRoot"
		},

		showLoginPage: function() {
			var loginPageView = new LoginPageView();
			loginPageView.render();
		},

		showUserDetails: function() {
			var userDetailsView = new UserDetailsView();
			userDetailsView.render();
		},

		showRoot: function(path) {
			console.warn('Warning: unknown path.. ', path);
			this.showLoginPage();
		}
	});

	var init = function() {
		new Router();
		
		Backbone.history.start();
	};

	return {
		init: init
	}
});